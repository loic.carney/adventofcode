package day02

import (
	"strings"

	"bufio"
	"os"

	"gitlab.com/loic.carney/adventofcode/helpers"
	"gitlab.com/loic.carney/adventofcode/model"

	log "github.com/sirupsen/logrus"
)

func ParseMatch(filePath string) model.Match {
	log.Info("Parsing ", filePath)

	f, err := os.Open(filePath)
	helpers.CheckError(err)

	defer f.Close()

	scanner := bufio.NewScanner(f)

	match := model.Match{}
	for scanner.Scan() {
		input := scanner.Text()

		if input != "" {
			opponent, me, _ := strings.Cut(input, " ")
			helpers.CheckError(err)
			match.AddGame(opponent, me)
		}
	}

	helpers.CheckError(scanner.Err())
	return match
}

func GetMeTotalScorePartOne(match model.Match) int {
	match.ScoresPartOne()
	return match.MeScore
}

func GetMeTotalScorePartTwo(match model.Match) int {
	match.ScoresPartTwo()
	return match.MeScore
}