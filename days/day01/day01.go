package day01

import (
	"sort"

	"bufio"
	"os"
	"strconv"

	"gitlab.com/loic.carney/adventofcode/helpers"
	"gitlab.com/loic.carney/adventofcode/model"

	log "github.com/sirupsen/logrus"
)

func ParseLutins(filePath string) []model.Lutin {
	log.Info("Parsing ", filePath)
	var lutins []model.Lutin

	f, err := os.Open(filePath)
	helpers.CheckError(err)

	defer f.Close()

	scanner := bufio.NewScanner(f)

	lutin := model.NewLutin()
	for scanner.Scan() {
		input := scanner.Text()

		if input == "" {
			lutins = append(lutins, *lutin)
			lutin = model.NewLutin()
		} else {
			v, err := strconv.Atoi(input)
			helpers.CheckError(err)
			lutin.AddFood(v)
		}
	}

	helpers.CheckError(scanner.Err())
	return lutins
}

func SortLutins(lutins []model.Lutin) []model.Lutin {
	sort.SliceStable(lutins, func(i, j int) bool {
		return lutins[i].Total() < lutins[j].Total()
	})
	return lutins
}

func GetLutinsMax(lutins []model.Lutin) int {
	return lutins[len(lutins)-1].Total()
}

func GetTop3LutinsMax(lutins []model.Lutin) int {
	sum := 0
	nbLutins := len(lutins)
	top3Lutins := lutins[nbLutins-3 : nbLutins]
	for _, l := range top3Lutins {
		sum += l.Total()
	}
	return sum
}
