package main

import (
	"os"

	"gitlab.com/loic.carney/adventofcode/days/day01"
	"gitlab.com/loic.carney/adventofcode/days/day02"

	log "github.com/sirupsen/logrus"
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)
	//log.SetReportCaller(true)
}

func main() {
	log.Info("Starting AdventOfCode 2022.")

	// ------
	// DAY 01
	filePathDay01 := os.Args[1]
	lutins := day01.ParseLutins(filePathDay01)
	sortedLutins := day01.SortLutins(lutins)
	lutinMax := day01.GetLutinsMax(sortedLutins)
	lutinsTop3 := day01.GetTop3LutinsMax(sortedLutins)
	log.Info("Day 1 : Lutin max '", lutinMax, "' et Top 3 Lutin max '", lutinsTop3, "' sur ", len(sortedLutins), " lutins")
	
	// ------
	// DAY 02
	filePathDay02 := os.Args[2]
	match := day02.ParseMatch(filePathDay02)
	meTotalScorePartOne := day02.GetMeTotalScorePartOne(match)
	meTotalScorePartTwo := day02.GetMeTotalScorePartTwo(match)
	log.Info("Day 2 : My score Part One '", meTotalScorePartOne)
	log.Info("Day 2 : My score Part Two '", meTotalScorePartTwo)

	// ------
	// DAY 03
	

	// ------
	log.Info("Ending AdventOfCode 2022.")
}
