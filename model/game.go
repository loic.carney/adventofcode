package model

type Match struct {
	games         []Game
	OpponentScore int
	MeScore       int
}
type Game struct {
	opponent      string
	me            string
	opponentScore int
	meScore       int
}

func NewMatch() *Match {
	match := Match{
		OpponentScore: 0,
		MeScore:       0,
	}
	return &match
}
func NewGame(opponent string, me string) *Game {
	game := Game{
		opponent:      opponent,
		me:            me,
		opponentScore: 0,
		meScore:       0,
	}
	return &game
}

func (m *Match) AddGame(opponent string, me string) {
	g := NewGame(opponent, me)
	m.games = append(m.games, *g)
}

func (m *Match) ScoresPartOne() {
	m.OpponentScore = 0
	m.MeScore = 0
	for _, g := range m.games {
		g.ScoresPartOne()
		m.OpponentScore += g.opponentScore
		m.MeScore += g.meScore
	}
}
func (m *Match) ScoresPartTwo() {
	m.OpponentScore = 0
	m.MeScore = 0
	for _, g := range m.games {
		g.ScoresPartTwo()
		m.OpponentScore += g.opponentScore
		m.MeScore += g.meScore
	}
}

func (g *Game) ScoresPartOne() {
	switch g.opponent {
	case "A":
		switch g.me {
		case "X":
			g.opponentScore = 3 + 1
			g.meScore = 3 + 1
		case "Y":
			g.opponentScore = 0 + 1
			g.meScore = 6 + 2
		case "Z":
			g.opponentScore = 6 + 1
			g.meScore = 0 + 3
		}
	case "B":
		switch g.me {
		case "X":
			g.opponentScore = 6 + 2
			g.meScore = 0 + 1
		case "Y":
			g.opponentScore = 3 + 2
			g.meScore = 3 + 2
		case "Z":
			g.opponentScore = 0 + 2
			g.meScore = 6 + 3
		}
	case "C":
		switch g.me {
		case "X":
			g.opponentScore = 0 + 3
			g.meScore = 6 + 1
		case "Y":
			g.opponentScore = 6 + 3
			g.meScore = 0 + 2
		case "Z":
			g.opponentScore = 3 + 3
			g.meScore = 3 + 3
		}
	}
}

func (g *Game) ScoresPartTwo() {
	switch g.opponent {
	case "A":
		switch g.me {
		case "X":
			g.opponentScore = 6 + 1
			g.meScore = 0 + 3
		case "Y":
			g.opponentScore = 3 + 1
			g.meScore = 3 + 1
		case "Z":
			g.opponentScore = 0 + 1
			g.meScore = 6 + 2
		}
	case "B":
		switch g.me {
		case "X":
			g.opponentScore = 6 + 2
			g.meScore = 0 + 1
		case "Y":
			g.opponentScore = 3 + 2
			g.meScore = 3 + 2
		case "Z":
			g.opponentScore = 0 + 2
			g.meScore = 6 + 3
		}
	case "C":
		switch g.me {
		case "X":
			g.opponentScore = 6 + 3
			g.meScore = 0 + 2
		case "Y":
			g.opponentScore = 3 + 3
			g.meScore = 3 + 3
		case "Z":
			g.opponentScore = 0 + 3
			g.meScore = 6 + 1
		}
	}
}
