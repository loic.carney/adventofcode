package model

type Lutin struct {
	Food []int
}

func NewLutin() *Lutin {
	lutin := Lutin{}
	return &lutin
}

func (l *Lutin) AddFood(value int) {
	l.Food = append(l.Food, value)
}

func (l *Lutin) Total() int {
	result := 0  
	for _, value := range l.Food {  
		result += value  
	}  
	return result
}